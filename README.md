# Kafka connect in Kubernetes

### Using terraform to prepare Azure Kubernetes Server (AKS) and it's infrastracture   

* Change directory to terraform directory
  ```bash
  pushd ./terraform
  ```

* Slightly change to terraform/main.tf file in `terraform.backend` block:  
You will be asked to add these parameters anyway, so it looks for me to be best practice to add them in main.tf instead of prompting it in Shell.  
These parameters are:  
  > name of the *resource group* for your azure account    

  > *storage account name* - to store terraform backend

* Initialize Terraform
  ```bash
  terraform init
  ```  

* Evaluate plan:
  ```bash
  terraform plan -out tfplan
  ```

* Apply plan:
  ```bash
  terraform apply "tfplan"
  ```

* to check if everything ready - you can go to *Azure Portal* and find *All Resources*.Terraform has created a set of resources, the most important one is *Azure Kubernetes Service (AKS)*. 

## Launch Confluent for Kubernetes

### Create a namespace

* Get credentials for AKS:
  ```
  az aks get-credentials --resource-group <AKS resource group> --name <AKS name>
  ```
 
* Attach docker image to AKS:
  ```
  az aks update -g <AKS resource group> -n <AKS name> --attach-acr <ACR name>
  ```
    
- Create the namespace to use:

  ```cmd
  kubectl create namespace confluent
  ```

- Set this namespace to default for your Kubernetes context:

  ```cmd
  kubectl config set-context --current --namespace confluent
  ```  

### Install Confluent for Kubernetes

- Add the Confluent for Kubernetes Helm repository:

  ```cmd
  helm repo add confluentinc https://packages.confluent.io/helm
  helm repo update
  ```
- Install Confluent for Kubernetes:

  ```cmd
  helm upgrade --install confluent-operator confluentinc/confluent-for-kubernetes
  ```

### Install Confluent Platform

- Install all Confluent Platform components:

  ```cmd
  kubectl apply -f ./confluent-platform.yaml
  ```

- Install a sample producer app and topic:

  ```cmd
  kubectl apply -f ./producer-app-data.yaml
  ```

- Check that everything is deployed:

  ```cmd
  kubectl get pods -o wide 
  ```
 

### View Control Center

- Set up port forwarding to Control Center web UI from local machine:

  ```cmd
  kubectl port-forward controlcenter-0 9021:9021
  ```

- Browse to Control Center: [http://localhost:9021](http://localhost:9021)
  ![screenshots](screenshots/m11-1.png)

* Investigate the cluster
  ![screenshots](screenshots/m11-2.png)

## Create a kafka topic

- The topic should have at least 3 partitions because the azure blob storage has 3 partitions. Name the new topic: "expedia".
  ![screenshots](screenshots/m11-3.png)
  ![screenshots](screenshots/m11-4.png)
  ![screenshots](screenshots/m11-5.png)

## Prepare the azure connector configuration

* azure connector configuration is stored in `connectors\azure-source-cc-expedia.json` file. It can be configured according [tutorial](https://docs.confluent.io/kafka-connect-azure-blob-storage-source/current/overview.html):
  
## Upload the connector file through the API
* The easiest way to upload configuration to confluent is to use UI.
* Click on Connect tab and "connect" cluster there: 
  ![screenshots](screenshots/m11-8.png)  
* Choose `upload connector using file` and browse to `connectors\azure-source-cc-expedia.json` file.
* If everything went ok you will see new connector `expedia` up and run:
  ![screenshots](screenshots/m11-9.png)  

* Examine it:
  ![screenshots](screenshots/m11-10.png)  

* Go to Topics > expedia > Messages to check the messages we get from connector:
  ![screenshots](screenshots/m11-11.png)  
Everything works!



